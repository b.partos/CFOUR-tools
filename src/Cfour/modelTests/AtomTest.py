'''
Created on Jul 1, 2018

@author: balazs
'''

# from Cfour.model.Atom import Atom
from Cfour.model.Coordinates import Coordinates

from Cfour.model import Atom


testAtom = Atom("C", Coordinates(2,5,9))

print(testAtom.coordinates.coordinatesToList())
print(testAtom.symbol)
testAtom2 = Atom("Ce", 5)
print(testAtom2.coordinates)