'''
Created on Jun 29, 2018

@author: balazs
'''

from Cfour.model.ExcitedState import ExcitedState
from Cfour.model.ExcitationVector import ExcitationVector


    
testExcitedState = ExcitedState(3.58 ,"CCSD")
print(testExcitedState)
testExcitationVector = ExcitationVector(0.26548, 14,25)
print(testExcitationVector)
print(testExcitationVector.coefficient)
print(type(testExcitationVector))
print(ExcitationVector)
print(type(testExcitationVector)==ExcitationVector)
testExcitedState.addExcitationVector(testExcitationVector)
print(testExcitedState.calculationLevel)

