'''
Created on Jun 18, 2018

@author: balazs
'''
from Cfour.model.OutputFile import OutputFile
from Cfour.model.OutputFileIndex import OutputFileIndex

class OutputFileReader(object):
  '''
  classdocs
  '''
  

  def __init__(self):
    '''
    Constructor
    '''
    self.__outputFile = None
  
  @property
  def outputFile(self):
    return self.outputFile
  
  @outputFile.seter
  def outputFile(self, outputFile):
    if outputFile.type() == OutputFile:
      self.outputFile = outputFile
    else:
      raise TypeError("Requires an Outputfile.")
  
  def validateOutputFile(self, outputfile):
    pass
  
  
  
  
