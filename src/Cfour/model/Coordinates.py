'''
Created on Apr 18, 2018

@author: balazs
'''

import array

class Coordinates:
  def __init__(self, x, y, z):
    self.values = array.array('d',[x,y,z])

  @property
  def x(self):
    return self.values[0]

  @property
  def y(self):
    return self.values[1]

  @property
  def z(self):
    return self.values[2]

    
  def coordinatesToList(self):
    return list(self.values)

