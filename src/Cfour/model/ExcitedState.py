'''
Created on Apr 30, 2018

@author: balazs
'''

from Cfour.model.ExcitationVector import ExcitationVector
import Cfour

class ExcitedState(object):
  '''
  classdocs
  '''


  def __init__(self, excitationEnergy, calculationLevel ):
    self.excitationVectors = []
    self.excitationEnergy = excitationEnergy
    self.calculationLevel = calculationLevel
  
  
  def addExcitationVector(self, excitationVector):
    if(type(excitationVector)==ExcitationVector):
      self.excitationVectors.append(excitationVector)
    else:
      raise TypeError("ExcitedState.addExcitationVector requires type ExcitationVector as an argument!")
  
      
      