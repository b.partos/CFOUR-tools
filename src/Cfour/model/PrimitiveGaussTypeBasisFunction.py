'''
Created on Jul 2, 2018

@author: balazs
'''

from Cfour.model.Coordinates import Coordinates
import array
import math

class PrimitiveGaussTypeBasisFunction(object):
  '''
  classdocs
  '''
  def __init__(self, coefficient, zeta):
    self.__parameters = array.array('d', [coefficient, zeta])
  
  def evaluate(self, coordinates):
    if(type(coordinates)==Coordinates):
      return self.coefficient*math.exp(-self.zeta*((coordinates.x)**2
                                                   +(coordinates.y)**2
                                                   +(coordinates.z)**2)**(0.5))
    else:
      raise ValueError
  
  @property
  def coefficient(self):
    return self.__parameters[0]
  
  @property
  def zeta(self):
    return self.__parameters[1]
  