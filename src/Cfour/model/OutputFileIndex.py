'''
Created on Apr 15, 2018

@author: balazs
'''
from Cfour.model.OutputFile import OutputFile

#!/usr/bin/env python3
# -*- coding: utf-8 -*-


class OutputFileIndex:

  def __init__(self, outputFile):
#     self.outputFileObject = OutputFile()
    self.outputFile = outputFile
    self.ZMAT_EndIndex = 0
    self.ZMAT_StartIndex = 0
    self.ZMAT_AtomicCoordinatesStart = 0
    self.ZMAT_AtomicCoordinatesEnd = 0
    self.ZMAT_ControlParametersStart = 0
    self.ZMAT_ControlParametersEnd = 0
    self.ZMAT_extern_potStart = 0
    self.ZMAT_extern_potEnd = 0
    self.ZMAT_exciteStart = 0
    self.ZMAT_exciteEnd = 0
    self.excitedStateStartIndexes = []
    self.controlParamtersStart = 0
    self.controlParamtersEnd = 0
    self.basisStart = 0
    self.basisEnd = 0
    self.SCF_Start = 0
    self.SCF_End = 0
    
    self.outputStringListParser_ZMAT()
    self.outputStringListParser_ControlParameters()
    self.outputStringListParser_Basis()
    self.outputStringListParser_SCF()
    self.outputStringListParser_ExcitedStates()

  def outputStringListParser_ZMAT(self):
    lineIndex = 0;
    while lineIndex < self.outputFileObject.wordListSize:
      if self.outputFileObject.wordList[lineIndex] == ['*', 'Input', 'from', 'ZMAT',
                                              'file', '*']:
        self.ZMAT_StartIndex = lineIndex + 2
        lineIndex += 3
        self.ZMAT_AtomicCoordinatesStart = lineIndex
        while self.outputFileObject.wordList[lineIndex] != []:
          lineIndex += 1
        self.ZMAT_AtomicCoordinatesEnd = lineIndex - 1
        while self.outputFileObject.wordList[lineIndex] == []:
          lineIndex += 1
        self.ZMAT_ControlParametersStart = lineIndex
        while self.outputFileObject.wordList[lineIndex] != []:
          lineIndex += 1
        self.ZMAT_ControlParametersEnd = lineIndex - 1
        while self.outputFileObject.wordList[lineIndex] != [
          "********************************************************************************"]:
          if self.outputFileObject.wordList[lineIndex] == ['%extern_pot*']:
            self.ZMAT_extern_potStart = lineIndex
            while self.outputFileObject.wordList[lineIndex] != []:
              lineIndex += 1
            self.ZMAT_extern_potEnd = lineIndex - 1
          elif self.outputFileObject.wordList[lineIndex] == ['%excite*']:
            self.ZMAT_exciteStart = lineIndex
            while self.outputFileObject.wordList[lineIndex] != []:
              lineIndex += 1
            self.ZMAT_exciteEnd = lineIndex - 1
          elif self.outputFileObject.wordList[lineIndex] == []:
            lineIndex += 1
          else:
            lineIndex += 1
        self.ZMAT_EndIndex = lineIndex - 1

      lineIndex += 1  # # Kell ez?

  def outputStringListParser_SCF(self):
    lineIndex = self.basisEnd + 3
    self.SCF_Start = lineIndex
    while self.outputFileObject.wordList[lineIndex] != ['VSCF', 'finished.']:
      lineIndex += 1
    self.SCF_End = lineIndex

  def outputStringListParser_ControlParameters(self):
    lineIndex = self.ZMAT_EndIndex + 8
    self.controlParamtersStart = self.ZMAT_EndIndex + 2
    while self.outputFileObject.wordList[lineIndex] != [
      '-------------------------------------------------------------------']:
      lineIndex += 1
    self.controlParamtersEnd = lineIndex - 1

  def outputStringListParser_Basis(self):
    lineIndex = self.controlParamtersEnd + 1
    while self.outputFileObject.wordList[lineIndex] != ['INPUT', 'FROM', 'MOL',
                                               'FILE']:
      lineIndex += 1
    lineIndex += 2
    self.basisStart = lineIndex
    while self.outputFileObject.wordList[lineIndex] != ['FINISH']:
      lineIndex += 1
    self.basisEnd = lineIndex

  def outputStringListParser_ExcitedStates(self):
    lineIndex = self.ZMAT_EndIndex + 1
    length = self.outputFileObject.wordListSize
    while lineIndex < self.outputFileObject.wordListSize:
      if self.outputFileObject.wordList[lineIndex][0:2] == ['Converged',
                                                   'eigenvalue:']:
        self.excitedStateStartIndexes.append(lineIndex)
        while ((lineIndex < length) and
               ((self.outputFileObject.wordList[lineIndex] != ['Adding', 'new',
                                                      'vector', 'to',
                                                      'expansion',
                                                      'space.']) and
                (self.outputFileObject.wordList[lineIndex] != ['Creating', 'initial',
                                                      'guess', 'for',
                                                      'root']))):
          lineIndex += 1

      lineIndex += 1


  
