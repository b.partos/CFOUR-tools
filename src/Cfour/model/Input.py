'''
Created on Apr 18, 2018

@author: balazs
'''

class Input:
  """
  Class to represent CFOUR input
  """
  
  def __init__(self):
    self.atomicCoordinates = [] # A list of Atom objects
    self.title = ""
    self.calculationParameters # A list of Calculation
    self.pointCharges
    self.searchVectors
