'''
Created on Apr 18, 2018

@author: balazs
'''
from Cfour.model.Coordinates import Coordinates

class Atom:
  
  def __init__(self, symbol, coordinates):
    if type(coordinates)==Coordinates:
      self.__symbol = symbol
      self.__coordinates = coordinates # Coordinates3D type
    else:
      raise ValueError("Requires Coordinate type as input")
      
  @property
  def symbol(self):
    return self.__symbol
  
  @property
  def coordinates(self):
    return self.__coordinates
  
#   def getCoordinatesAsTuple(self):
#     return (self.coordinates.getX(), self.coordinates.getY(), self.coordinates.getZ())
  
