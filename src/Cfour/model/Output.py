'''
Created on Apr 30, 2018

@author: balazs
'''
from Cfour.model.OutputFileIndex import OutputFileIndex

class Output(object):
  '''
  classdocs
  '''


  def __init__(self, outputFileRelativePath):
    '''
    The constructor should receive the relative path of the output
    file.
    '''
    
    with open(outputFileRelativePath) as outputFileObject:
      outputFileString = outputFileObject.read()
    
    self.outputFileIndex = OutputFileIndex(outputFileString)
    
    