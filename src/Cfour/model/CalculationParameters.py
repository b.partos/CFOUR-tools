'''
Created on Apr 24, 2018

@author: balazs
'''

class CalculationParamters(object):

  
  
  def __init__(self):
    self.parameters = []
    self.values = []
    
  
  
  def addParameter(self, parameterName, value):
    '''
    Adds the parameter and the value to the lists if the parameter has not been set yet
    If the parameter has already been set, it overwrites its current value with the new one.
    '''
    if(self.parameters.count(parameterName)==0):
      self.parameters.append(parameterName)
      self.values.append(value)
    else:
      self.values[self.parameters.index(parameterName)]=value
    
  def removeParamterByName(self, parameterName):
    if(self.parameters.count(parameterName)>0):
      parameterIndex = self.parameters.index(parameterName)
      self.parameters.remove(self.parameters[parameterIndex])
      self.values.remove(self.values[parameterIndex])
    
  
      