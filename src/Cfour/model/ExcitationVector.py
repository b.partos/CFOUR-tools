'''
Created on Apr 28, 2018

@author: balazs
'''

class ExcitationVector(object):
  '''
  A class to represent excitations
  '''


  def __init__(self, coefficient ,i, a, j=0, b=0, k=0, c=0):
    '''
    Constructor
    '''
    self.__coefficient=coefficient
    self.__a = a
    self.__b = b
    self.__c = c
    self.__i = i
    self.__j = j
    self.__k = k
    
  
  @property
  def coefficient(self):
    return self.__coefficient
  
  
  
  def getTransition1(self):
    return (self.__i, self.__a)
  
  def getTransition2(self):
    return (self.__j, self.__b)
  
  def getTransition3(self):
    return (self.__k, self.__c)
  
  
  